package com.example.viniciusns.navegador;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Navegador extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navegador);

        //Recupera o String passado na Intent
        String url = getIntent().getDataString();

        if(null == url)
            url = "Nenhuma URL informada";

        //Coloca o String no Textview
        TextView textView = (TextView) findViewById(R.id.url);
        textView.setText(url);
    }
}
